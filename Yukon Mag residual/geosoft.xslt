<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:geo="http://www.geosoft.com/schema/geo">
   <xsl:output method="html" encoding="utf-8"/>
   <xsl:template match="/">
      <html>
         <head>
            <style type="text/css">
               hr.thick_seperator {color: rgb(0,0,204)}
               hr.thin_seperator {
               color: rgb(0,0,255);
               height: 1;
               }

               table.summary {
               background-color: rgb(226,238,255);
               border: 1;
               border-color: rgb(0,0,255);
               border-style: solid;
               width: 100%;
               cellpadding: 8;
               cellspacing: 8;
               }

               table.lineage {
               margin-left: 15;
               border-collapse: collapse;
               background-color: rgb(226,238,255);
               }

               tr.lineageHeading {
               font-family: arial;
               color: rgb(255, 255, 255);
               font-size: 11;
               font-weight: bold;
               background-color: rgb(0,0,204);
               }

               th.lineageHeader {
               border: 1;
               border-width: 1;
               border-color: rgb(255, 255, 255);
               border-style: solid;
               }

               tr.lineageRow {
               font-family: arial;
               color: rgb(0, 0, 204);
               font-size: 11;
               }

               td.lineage{
               border: 1;
               border-width: 1;
               border-color: rgb(255, 255, 255);
               border-style: solid;
               }

               span.label {
               font-family: arial;
               color: rgb(0,0,0);
               font-size: 11;
               font-weight: bold;
               }

               span.text {
               font-family: arial;
               color: rgb(0,0,204);
               font-size: 11;
               }

               a.text {
               font-family: arial;
               color: rgb(0,0,204);
               font-size: 11;
               text-decoration: underline
               }

               span.category {
               font-family: arial;
               color: rgb(0,0,204);
               font-size: 11;
               font-weight: bold;
               font-style: italic;
               }

               a.section {
               font-family: arial;
               font-size: 11;
               color: rgb(0,0,204);
               font-style: italic;
               text-decoration: underline
               }

               a.backtotop {
               font-family: arial;
               font-size: 8;
               color: rgb(0,0,204);
               text-decoration: underline
               }

               div.title {
               margin-bottom: 5;
               cursor: hand;
               }

               div.level1 {
               margin-left: 15;
               }
               div.level1Title {
               margin-left: 15;
               margin-bottom: 5;
               }

               div.level1TitleWithSpacingAbove {
               margin-left: 15;
               margin-bottom: 5;
               margin-top: 5;
               }

               div.level2 {
               margin-left: 25;
               }

               div.spacer {
               margin-bottom: 10;
               }

               div.section {
               display: block;
               }
            </style>
            <script type="text/javascript">
               function ToggleSection(strSectionId) {
               oSection = document.getElementById(strSectionId);
               oSectionStatus = document.getElementById(strSectionId + "Status");
               if (oSection.style.display == "block" || oSection.style.display == "") {
               oSection.style.display = "none";
               oSectionStatus.innerHTML = "+  ";
               } else {
               oSection.style.display = "block";
               oSectionStatus.innerHTML = "-  ";
               }
               }
            </script>
         </head>
         <body>
            <a id="top"/>
            <!-- show metadata summary -->

            <xsl:if test="//geo:geosoft/geo:dataset/geo:title/text() | //geo:geosoft/geo:dataset/geo:project_name/text() | //geo:geosoft/geo:dataset/geo:description/text()">
               <div class="title">
                  <table class="summary">
                     <xsl:if test="//geo:geosoft/geo:dataset/geo:title/text()">
                        <tr>
                           <td>
                              <span class="label">Title:  </span>
                              <span class="text">
                                 <xsl:value-of select="//geo:geosoft/geo:dataset/geo:title/text()"/>
                              </span>
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="//geo:geosoft/geo:dataset/geo:project_name/text()">
                        <tr>
                           <td>
                              <span class="label">Project name:  </span>
                              <span class="text">
                                 <xsl:value-of select="//geo:geosoft/geo:dataset/geo:project_name/text()"/>
                              </span>
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="//geo:geosoft/geo:dataset/geo:description/text()">
                        <tr>
                           <td>
                              <span class="label">Abstract:  </span>
                              <span class="text">
                                 <xsl:value-of select="//geo:geosoft/geo:dataset/geo:description/text()"/>
                              </span>
                           </td>
                        </tr>
                     </xsl:if>
                  </table>
               </div>
            </xsl:if>
            <div>
               <span class="category">Metadata:</span>
            </div>
            <xsl:if test="//geo:geosoft/geo:dataset/geo:continent/text() | //geo:geosoft/geo:dataset/geo:country/text() | //geo:geosoft/geo:dataset/geo:province_state/text() | //geo:geosoft/geo:dataset/geo:region/text()">
               <div class="level1">
                  <a class="section" href="#Location">Location</a>
               </div>
            </xsl:if>
            <xsl:if test="//geo:geosoft/geo:dataset/geo:abstract/text() | //geo:geosoft/geo:dataset/geo:purpose/text() | //geo:geosoft/geo:dataset/geo:category/text() | //geo:geosoft/geo:dataset/geo:subject/text() | //geo:geosoft/geo:dataset/geo:type/text() | //geo:geosoft/geo:dataset/geo:file_format/text() | //geo:geosoft/geo:dataset/geo:weblink">
               <div class="level1">
                  <a class="section" href="#Description">Description</a>
               </div>
            </xsl:if>
            <xsl:if test="//geo:geosoft/geo:dataset/geo:data_source/text() | //geo:geosoft/geo:dataset/geo:confidentiality/text() | //geo:geosoft/geo:dataset/geo:project_name/text() | //geo:geosoft/geo:dataset/geo:project_type/text() | //geo:geosoft/geo:dataset/geo:deposit_type/text() | //geo:geosoft/geo:dataset/geo:resolution/text() | //geo:geosoft/geo:dataset/geo:data_created_by/text() | //geo:geosoft/geo:dataset/geo:data_creation_date/text()">
               <div class="level1">
                  <a class="section" href="#Data">Data</a>
               </div>
            </xsl:if>
            <xsl:if test="//geo:geosoft/geo:dataset/geo:georeference">
               <div class="level1">
                  <a class="section" href="#Projection">Coordinate System</a>
               </div>
            </xsl:if>
            <xsl:if test="//geo:geosoft/geo:dataset/geo:metadata_created_by/text() | //geo:geosoft/geo:dataset/geo:metadata_creation_date/text()">
               <div class="level1">
                  <a class="section" href="#MetadataFile">Metadata File</a>
               </div>
            </xsl:if>
            <xsl:if test="//geo:geosoft/geo:dataset/geo:lineage">
               <div class="level1">
                  <a class="section" href="#Lineage">Lineage</a>
               </div>
            </xsl:if>
            <hr class="thick_seperator"/>
            <xsl:apply-templates select="//geo:geosoft/geo:dataset" mode="Main"/>
            <xsl:apply-templates select="//geo:geosoft/geo:dataset/geo:georeference"/>
            <xsl:apply-templates select="//geo:geosoft/geo:dataset" mode="Secondary"/>
            <xsl:apply-templates select="//geo:geosoft/geo:dataset/geo:lineage"/>
         </body>
      </html>
   </xsl:template>

   <xsl:template name="MainInformation" match="geo:dataset" mode="Main">
            <!-- Location Information -->
      <xsl:if test="geo:continent/text() | geo:country/text() | geo:province_state/text() | geo:region/text()">
         <a id="Location"/>
         <div class="title" onclick="javascript:ToggleSection('LocationSection');">
            <span id="LocationSectionStatus">-  </span>
            <span class="category">Location</span>
         </div>
         <div id="LocationSection" class="section">
            <xsl:if test="geo:continent/text()">
               <div class="level1">
                  <span class="label">Continent:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:continent/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:country/text()">
               <div class="level1">
                  <span class="label">Country:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:country/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:province_state/text()">
               <div class="level1">
                  <span class="label">Province/State:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:province_state/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:region/text()">
               <div class="level1">
                  <span class="label">Region:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:region/text()"/>
                  </span>
               </div>
            </xsl:if>
         </div>
         <div class="spacer"/>
         <a class="backtotop" href="#top">Back To Top</a>
         <hr class="thin_seperator"/>
      </xsl:if>

      <xsl:if test="geo:purpose/text() | geo:category/text() | geo:subject/text() | geo:type/text() | geo:file_format/text() | geo:weblink/text()">
         <a id="Description"/>
         <div class="title" onclick="javascript:ToggleSection('DescriptionSection');">
            <span id="DescriptionSectionStatus">-  </span>
            <span class="category">Description</span>
         </div>
         <div id="DescriptionSection">
            <xsl:if test="geo:purpose/text()">
               <div class="level1">
                  <span class="label">Purpose:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:purpose/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:subject/text()">
               <div class="level1">
                  <span class="label">Primary Theme/Subject:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:subject/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:category/text()">
               <div class="level1">
                  <span class="label">Secondary Theme/Category:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:category/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:type/text()">
               <div class="level1">
                  <span class="label">Tertiary Theme/Type:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:type/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:file_format/text()">
               <div class="level1">
                  <span class="label">File Format:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:file_format/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:weblink/text()">
               <div class="level1">
                  <span class="label">Web Link:  </span>
                  <a class="text" target="_blank">
                     <xsl:attribute name="href">
                        <xsl:value-of select="geo:weblink/text()"/>
                     </xsl:attribute>
                     <xsl:value-of select="geo:weblink/text()"/>
                  </a>
               </div>
            </xsl:if>
         </div>
         <div class="spacer"/>
         <a class="backtotop" href="#top">Back To Top</a>
         <hr class="thin_seperator"/>
      </xsl:if>

      <!-- Data Information -->
      <xsl:if test="geo:data_source/text() | geo:confidentiality/text() | geo:project_name/text() | geo:project_type/text() | geo:deposit_type/text() | geo:resolution/text() | geo:data_created_by/text() | geo:data_creation_date/text()">
         <a id="Data"/>
         <div class="title" onclick="javascript:ToggleSection('DataSection');">
            <span id="DataSectionStatus">-  </span>
            <span class="category">Data</span>
         </div>
         <div id="DataSection">
            <xsl:if test="geo:data_source/text()">
               <div class="level1">
                  <span class="label">Data Source:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:data_source/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:confidentiality/text()">
               <div class="level1">
                  <span class="label">Confidentiality:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:confidentiality/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:project_name/text()">
               <div class="level1">
                  <span class="label">Project Name:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:project_name/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:project_type/text()">
               <div class="level1">
                  <span class="label">Project Type:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:project_type/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:deposit_type/text()">
               <div class="level1">
                  <span class="label">Deposit Type:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:deposit_type/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:resolution/text()">
               <div class="level1">
                  <span class="label">Scale/Resolution:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:resolution/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:data_created_by/text()">
               <div class="level1">
                  <span class="label">Created By:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:data_created_by/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:data_creation_date/text()">
               <div class="level1">
                  <span class="label">Creation Date:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:data_creation_date/text()"/>
                  </span>
               </div>
            </xsl:if>
         </div>
         <div class="spacer"/>
         <a class="backtotop" href="#top">Back To Top</a>
         <hr class="thin_seperator"/>
      </xsl:if>            
   </xsl:template>

   <xsl:template name="ProjectionInformation" match="geo:georeference">

      <!-- Projection Information -->

      <a id="Projection"/>
      <div class="title" onclick="javascript:ToggleSection('ProjectionSection');">
         <span id="ProjectionSectionStatus">-  </span>
         <span class="category">Coordinate System</span>
      </div>
      <div id="ProjectionSection">
         <xsl:if test="geo:projection/@type">
            <div class="level1">
               <span class="label">Type:  </span>
               <span class="text">
                  <xsl:value-of select="geo:projection/@type"/>
               </span>
            </div>
         </xsl:if>
         <xsl:if test="geo:projection/@name">
            <div class="level1">
               <span class="label">Name:  </span>
               <span class="text">
                  <xsl:value-of select="geo:projection/@name"/>
               </span>
            </div>
         </xsl:if>
         <xsl:if test="geo:projection/geo:units/@name">
            <div class="level1">
               <span class="label">Units:  </span>
               <span class="text">
                  <xsl:value-of select="geo:projection/geo:units/@name"/>
               </span>
            </div>
         </xsl:if>
         <xsl:if test="geo:projection/@datumtrf_description">
            <div class="level1">
               <span class="label">Local Datum:  </span>
               <span class="text">
                  <xsl:value-of select="geo:projection/@datumtrf_description"/>
               </span>
            </div>
         </xsl:if>
         <div class="level1TitleWithSpacingAbove">
            <span class="label">Extents</span>
         </div>
         <xsl:if test="geo:dataextents/geo:extent2d">
            <xsl:if test="geo:dataextents/geo:extent2d/@minx">
               <div class="level2">
                  <span class="label">Min X:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent2d/@minx"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent2d/@miny">
               <div class="level2">
                  <span class="label">Min Y:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent2d/@miny"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent2d/@maxx">
               <div class="level2">
                  <span class="label">Max X:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent2d/@maxx"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent2d/@maxy">
               <div class="level2">
                  <span class="label">Max Y:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent2d/@maxy"/>
                  </span>
               </div>
            </xsl:if>
         </xsl:if>
         <xsl:if test="geo:dataextents/geo:extent3d">
            <xsl:if test="geo:dataextents/geo:extent3d/@minx">
               <div class="level2">
                  <span class="label">Min X:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent3d/@minx"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent3d/@miny">
               <div class="level2">
                  <span class="label">Min Y:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent3d/@miny"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent3d/@minz">
               <div class="level2">
                  <span class="label">Min Z:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent3d/@minz"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent3d/@maxx">
               <div class="level2">
                  <span class="label">Max X:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent3d/@maxx"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent3d/@maxy">
               <div class="level2">
                  <span class="label">Max Y:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent3d/@maxy"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:dataextents/geo:extent3d/@maxz">
               <div class="level2">
                  <span class="label">Max Z:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:dataextents/geo:extent3d/@maxz"/>
                  </span>
               </div>
            </xsl:if>
         </xsl:if>
      </div>
      <div class="spacer"/>
      <a class="backtotop" href="#top">Back To Top</a>
      <hr class="thin_seperator"/>
   </xsl:template>

   <xsl:template name="MetadataInfo" match="geo:dataset" mode="Secondary">
      <!-- Metadata File -->

      <xsl:if test="geo:metadata_created_by/text() | geo:metadata_creation_date/text()">
         <a id="MetadataFile"/>
         <div class="title" onclick="javascript:ToggleSection('MetadataFileSection');">
            <span id="MetadataFileSectionStatus">-  </span>
            <span class="category">Metadata File</span>
         </div>
         <div id="MetadataFileSection">
            <xsl:if test="geo:metadata_created_by/text()">
               <div class="level1">
                  <span class="label">Entered By:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:metadata_created_by/text()"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="geo:metadata_creation_date/text()">
               <div class="level1">
                  <span class="label">Entered Date:  </span>
                  <span class="text">
                     <xsl:value-of select="geo:metadata_creation_date/text()"/>
                  </span>
               </div>
            </xsl:if>
         </div>
         <div class="spacer"/>
         <a class="backtotop" href="#top">Back To Top</a>
         <hr class="thin_seperator"/>
      </xsl:if>
   </xsl:template>
   <xsl:template name="Linage" match="geo:lineage">
      <!-- Linage Information -->
      <a id="Lineage"/>
      <div class="title" onclick="javascript:ToggleSection('LineageSection');">
         <span id="LineageSectionStatus">-  </span>
         <span class="category">Lineage</span>
      </div>
      <div id="LineageSection">
         <xsl:for-each select="geo:process">
            <div class="level1">
               <span class="label">Process:  </span>
               <span class="text">
                  <xsl:value-of select="@name"/>
               </span>
            </div>
            <xsl:if test="@displaystring">
               <div class="level1">
                  <span class="label">Name:  </span>
                  <span class="text">
                     <xsl:value-of select="@displaystring"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="@description">
               <div class="level1">
                  <span class="label">Description:  </span>
                  <span class="text">
                     <xsl:value-of select="@description"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="@user">
               <div class="level1">
                  <span class="label">User:  </span>
                  <span class="text">
                     <xsl:value-of select="@user"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="@version">
               <div class="level1">
                  <span class="label">Version:  </span>
                  <span class="text">
                     <xsl:value-of select="@version"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="@date">
               <div class="level1">
                  <span class="label">Date:  </span>
                  <span class="text">
                     <xsl:value-of select="@date"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="@time">
               <div class="level1">
                  <span class="label">Time:  </span>
                  <span class="text">
                     <xsl:value-of select="@time"/>
                  </span>
               </div>
            </xsl:if>
            <xsl:if test="./geo:parameter">
               <table class="lineage">
                  <tr class="lineageHeading">
                     <th class="lineageHeader">Parameter Name</th>
                     <th class="lineageHeader">Value</th>
                  </tr>
                  <xsl:for-each select="./geo:parameter">
                     <tr class="lineageRow">
                        <td class="lineage">
                           <xsl:value-of select="@name"/>
                        </td>
                        <td class="lineage">
                           <xsl:value-of select="@value"/>
                        </td>
                     </tr>
                  </xsl:for-each>
               </table>
            </xsl:if>

            <xsl:if test="./geo:source">
               <table class="lineage">
                  <tr class="lineageHeading">
                     <th class="lineageHeader">Source Name</th>
                     <th class="lineageHeader">Type</th>
                  </tr>
                  <xsl:for-each select="./geo:source">
                     <tr class="lineageRow">
                        <td class="lineage">
                           <xsl:value-of select="@name"/>
                        </td>
                        <td class="lineage">
                           <xsl:value-of select="@type"/>
                        </td>
                     </tr>
                  </xsl:for-each>
               </table>
            </xsl:if>
            <div class="spacer"/>
         </xsl:for-each>
      </div>
      <div class="spacer"/>
      <a class="backtotop" href="#top">Back To Top</a>
      <hr class="thin_seperator"/>
   </xsl:template>
</xsl:stylesheet>

